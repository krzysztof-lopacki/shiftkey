package com.shiftkey.codingchallenge.availableshiftsbrowser.config

interface Configuration {
    val shiftsApiUrl: String
    val location: String
}