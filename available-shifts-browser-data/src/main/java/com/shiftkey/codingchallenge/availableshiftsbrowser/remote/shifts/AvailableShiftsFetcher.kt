package com.shiftkey.codingchallenge.availableshiftsbrowser.remote.shifts

import com.shiftkey.codingchallenge.availableshiftsbrowser.config.Configuration
import com.shiftkey.codingchallenge.availableshiftsbrowser.models.Day
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import java.util.*
import javax.inject.Inject
import javax.inject.Named

class AvailableShiftsFetcher @Inject constructor(
    configuration: Configuration,
    okHttpClient: OkHttpClient,
    @Named(ShiftsApi.API_NAME) responseParser: Converter.Factory
) {
    private val shiftsApi: ShiftsApi

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(configuration.shiftsApiUrl)
            .addConverterFactory(responseParser)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(okHttpClient)
            .build()
        shiftsApi = retrofit.create(ShiftsApi::class.java)
    }

    fun getAvailableShiftsForAWeek(
        firstDayToFetch: Calendar,
        address: String
    ): Single<List<Day>> {
        val firstDayString = ShiftsApi.DATE_FORMAT.format(firstDayToFetch.time)
        val firstDay = Calendar.getInstance().apply { time = ShiftsApi.DATE_FORMAT.parse(firstDayString)!! }
        return shiftsApi
            .getAvailableShifts(
                date = firstDayString,
                address = address
            )
            .map { response ->
                response.data.filter { day -> !day.date.before(firstDay) }
            }
    }
}

