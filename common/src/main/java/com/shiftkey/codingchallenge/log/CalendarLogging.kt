package com.shiftkey.codingchallenge.log

import java.text.SimpleDateFormat
import java.util.*

private val DATE_AND_TIME = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
    .apply { timeZone = TimeZone.getTimeZone("US/Central") }


fun Calendar.toLogString(): String {
    return DATE_AND_TIME.format(time)
}